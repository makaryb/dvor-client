package dev.dvor.theming

import androidx.compose.runtime.Composable

@Composable
actual fun isSystemInDarkTheme(): Boolean = androidx.compose.foundation.isSystemInDarkTheme()
