plugins {
    `kotlin-dsl`
    `java-gradle-plugin`
}

group = "dev.dvor.internal"
version = "0.1.0"

repositories {
    mavenCentral()
}

gradlePlugin {
    plugins.register("internal") {
        id = "internal"
        implementationClass = "dev.dvor.internal.InternalPlugin"
    }
}
