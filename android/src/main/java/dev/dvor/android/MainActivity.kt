package dev.dvor.android

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dev.dvor.theming.DvorTheme

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)

        setContent {
            DvorTheme {
                Scaffold {
                    Button(
                        modifier = Modifier,
                        onClick = {
                            //
                        },
                        enabled = true
                    ) {
                        Text(text = "Hello Multiplatform!")
                    }
                }
            }
        }
    }
}
