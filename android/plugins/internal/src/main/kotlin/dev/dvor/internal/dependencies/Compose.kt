/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal.dependencies

import dev.dvor.internal.Versions

object Compose {

    const val snapshot = ""

    @get:JvmStatic
    val snapshotUrl: String
        get() = "https://androidx.dev/snapshots/builds/$snapshot/artifacts/repository/"

    /**
     * [Compose Activity](https://androidx.tech/artifacts/activity/activity-compose/)
     */
    const val activity = "androidx.activity:activity-compose:${Versions.activityCompose}"

    /**
     * [Compose ViewModel](https://androidx.tech/artifacts/lifecycle/lifecycle-viewmodel-compose/)
     */
    const val viewModel =
        "androidx.lifecycle:lifecycle-viewmodel-compose:${Versions.viewModelCompose}"

    /**
     * [Navigation Compose](https://developer.android.com/jetpack/androidx/releases/navigation)
     */
    const val navigation = "androidx.navigation:navigation-compose:${Versions.navigationCompose}"

    /**
     * [Compose ConstraintLayout](https://developer.android.com/jetpack/compose/layouts/constraintlayout)
     */
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout-compose:${Versions.constraintLayoutCompose}"

    /**
     * [Paging Compose](https://developer.android.com/jetpack/androidx/releases/paging)
     */
    const val paging = "androidx.paging:paging-compose:${Versions.pagingCompose}"

    /**
     * [Compose Animation](https://developer.android.com/jetpack/compose/animation)
     */
    const val animation = "androidx.compose.animation:animation:${Versions.compose}"

    /**
     * [Compose Foundation](https://developer.android.com/jetpack/androidx/releases/compose-foundation)
     */
    const val foundation = "androidx.compose.foundation:foundation:${Versions.compose}"

    /**
     * [Compose ViewBinding](https://mvnrepository.com/artifact/androidx.compose.ui/ui-viewbinding?repo=google)
     */
    const val viewBinding = "androidx.compose.ui:ui-viewbinding:${Versions.compose}"

    /**
     * [Compose Runtime](https://developer.android.com/jetpack/androidx/releases/compose-runtime)
     */
    const val runtime = "androidx.compose.runtime:runtime:${Versions.compose}"
    const val runtimeLivedata = "androidx.compose.runtime:runtime-livedata:${Versions.compose}"

    /**
     * [Compose rxJava](https://developer.android.com/reference/kotlin/androidx/compose/runtime/rxjava2/package-summary)
     */
    const val rxJava2 = "androidx.compose.runtime:runtime-rxjava2:${Versions.compose}"

    /**
     * [Compose Material Components](https://mvnrepository.com/artifact/androidx.compose.material/material)
     */
    const val material = "androidx.compose.material:material:${Versions.compose}"

    /**
     * [Material Icons Core](https://developer.android.com/reference/kotlin/androidx/compose/material/icons)
     */
    const val materialIconsCore =
        "androidx.compose.material:material-icons-core:${Versions.compose}"

    /**
     * [Material Icons Extended by Infragistics](https://github.com/IgniteUI/material-icons-extended)
     */
    const val icons = "androidx.compose.material:material-icons-extended:${Versions.compose}"

    /**
     * [Compose UI Primitives](https://developer.android.com/jetpack/androidx/releases/compose-ui)
     */
    const val ui = "androidx.compose.ui:ui:${Versions.compose}"

    /**
     * [Compose UI Utils](https://developer.android.com/jetpack/androidx/releases/compose-ui)
     */
    const val uiUtil = "androidx.compose.ui:ui-util:${Versions.compose}"

    /**
     * [Compose Layouts](https://mvnrepository.com/artifact/androidx.compose.foundation/foundation-layout)
     */
    const val layout = "androidx.compose.foundation:foundation-layout:${Versions.compose}"

    /**
     * [Compose tooling](https://developer.android.com/jetpack/compose/tooling)
     */
    const val tooling = "androidx.compose.ui:ui-tooling:${Versions.compose}"

    /**
     * [Navigation Compose Hilt Extension](https://mvnrepository.com/artifact/androidx.hilt/hilt-navigation-compose)
     */
    const val hiltNavigation =
        "androidx.hilt:hilt-navigation-compose:${Versions.hiltComposeNavigation}"

    /**
     * [UI test Compose](https://developer.android.com/jetpack/compose/testing)
     */
    const val uiTest = "androidx.compose.ui:ui-test:${Versions.compose}"
    const val uiTestJunit4 = "androidx.compose.ui:ui-test-junit4:${Versions.compose}"

    /**
     * [UI Test Manifest](https://developer.android.com/jetpack/compose/testing#setup)
     */
    const val uiTestManifest = "androidx.compose.ui:ui-test-manifest:${Versions.compose}"
}
