/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal

import dev.dvor.internal.dependencies.Accompanist
import dev.dvor.internal.dependencies.AndroidX
import dev.dvor.internal.dependencies.Compose
import dev.dvor.internal.dependencies.Coroutines
import dev.dvor.internal.dependencies.Di
import dev.dvor.internal.dependencies.Firebase
import dev.dvor.internal.dependencies.Google
import dev.dvor.internal.dependencies.Logging
import dev.dvor.internal.dependencies.Osmdroid
import dev.dvor.internal.dependencies.Other
import dev.dvor.internal.dependencies.PdfTron
import dev.dvor.internal.dependencies.Retrofit
import dev.dvor.internal.dependencies.Room

object Dependencies {
    val android = Android
    val compose = Compose
    val accompanist = Accompanist
    val hilt = Di
    val room = Room
    val retrofit = Retrofit
    val other = Other
    val androidX = AndroidX
    val coroutines = Coroutines
    val firebase = Firebase
    val google = Google
    val logging = Logging
    val osmdroid = Osmdroid
    val pdfTron = PdfTron
}
