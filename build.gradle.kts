// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {

    val hiltVersion = "2.36"
    val gradleVersion = "7.1.0-alpha03"
    val googleServicesVersion = "4.3.5"
    val crashlyticsGradleVersion = "2.7.1"

    repositories {
        mavenCentral()
        google()
        jcenter()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
    dependencies {
        classpath("com.android.tools.build:gradle:$gradleVersion")
        classpath("org.jetbrains.compose:compose-gradle-plugin:${Versions.jbCompose}")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}")

        classpath(kotlin("serialization", version = Versions.kotlin))

        classpath("com.google.dagger:hilt-android-gradle-plugin:$hiltVersion")

        classpath("com.google.gms:google-services:$googleServicesVersion")
        classpath("com.google.firebase:firebase-crashlytics-gradle:$crashlyticsGradleVersion")
    }
}

group = "dev.dvor"
version = "0.9.0"

plugins {
    id("io.gitlab.arturbosch.detekt") version "1.15.0"
    id("com.diffplug.spotless") version "5.10.0"

    id("internal")
}

detekt {
    // fail build on any finding
    failFast = true
    // preconfigure defaults
    buildUponDefaultConfig = true
    // point to your custom config defining rules to run, overwriting default behavior
    config = files("$projectDir/config/detekt.yml")
    // a way of suppressing issues before introducing detekt
    baseline = file("$projectDir/config/baseline.xml")

    reports {
        // observe findings in your browser with structure and code snippets
        html {
            enabled = true
        }
        // checkstyle like format mainly for integrations like Jenkins
        xml {
            enabled = true
        }
        // similar to the console output, contains issue signature to manually edit baseline files
        txt {
            enabled = true
        }
        // SARIF integration (https://sarifweb.azurewebsites.net/) for integrations with Github
        sarif {
            enabled = true
        }
    }
}

tasks.withType<io.gitlab.arturbosch.detekt.Detekt>().configureEach {
    // Target version of the generated JVM bytecode. It is used for type resolution.
    this.jvmTarget = "1.8"
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        maven("https://jitpack.io")
        maven("https://pdftron-maven.s3.amazonaws.com/release")
    }
}

object KtlintVersions {
    const val ktlint = "0.40.0"
    const val ktlintReporter = "0.2.1"
}

val ktlint by configurations.creating

configurations {
    ktlint
}

dependencies {
    ktlint("com.pinterest:ktlint:${KtlintVersions.ktlint}")
    ktlint("pm.algirdas.ktlint:reporter:${KtlintVersions.ktlintReporter}")
}

tasks.register<JavaExec>("ktlint") {
    group = "verification"
    description = "Check Kotlin code style."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args("--android", "--reporter=gitlab-quality,output=gl-code-quality-report.json")
    // to generate report in checkstyle format prepend following args:
    // "--reporter=plain", "--reporter=checkstyle,output=${buildDir}/ktlint.xml"
    // to add a baseline to check against prepend following args:
    // "--baseline=ktlint-baseline.xml"
    // see https://github.com/pinterest/ktlint#usage for more
}

tasks.named("check") {
    dependsOn(tasks.named("ktlint"))
}

tasks.register<JavaExec>("ktlintFormat") {
    group = "formatting"
    description = "Fix Kotlin code style deviations."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args("--android", "-F", "src/**/*.kt")
}

subprojects {
    repositories {
        google()
        mavenCentral()
        jcenter()
        maven("https://jitpack.io")
        maven("https://oss.sonatype.org/content/repositories/snapshots")
        maven(url = dev.dvor.internal.dependencies.Compose.snapshotUrl)
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        kotlinOptions {
            // Treat all Kotlin warnings as errors
            allWarningsAsErrors = false

            val options = listOf(
                "-Xopt-in=kotlin.RequiresOptIn",
                "-Xopt-in=kotlinx.coroutines.ExperimentalCoroutinesApi",
                "-Xopt-in=kotlinx.coroutines.FlowPreview",
                "-Xopt-in=kotlin.Experimental",
                "-P",
                "plugin:androidx.compose.compiler.plugins.kotlin:" +
                    "suppressKotlinVersionCompatibilityCheck=true"
            )
            freeCompilerArgs = freeCompilerArgs + options

            // Set JVM target to 1.8
            jvmTarget = "1.8"
        }
    }
}
