object Versions {
    const val kotlin = "1.4.32"
    const val compose = "1.0.0-beta06"
    const val jbCompose = "0.4.0-build179"
    const val accompanist = "0.7.0"

    const val datetime = "0.1.1"

    const val retrofit = "2.9.0"
    const val moshi = "2.4.0"
    const val okhttp = "4.9.1"
}

object Dependencies {
    const val datetime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.datetime}"
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val moshi = "com.squareup.retrofit2:converter-moshi:${Versions.moshi}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"

    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"

    object Compose {
        const val uiTooling = "androidx.compose.ui:ui-tooling:${Versions.compose}"
    }

    object Accompanist {
        const val coil = "com.google.accompanist:accompanist-coil:${Versions.accompanist}"
        const val uiController =
            "com.google.accompanist:accompanist-systemuicontroller:${Versions.accompanist}"
    }
}
