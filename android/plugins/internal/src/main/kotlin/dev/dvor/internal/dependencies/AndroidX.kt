/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal.dependencies

import dev.dvor.internal.Versions

object AndroidX {

    /**
     * [AppCompat](https://developer.android.com/jetpack/androidx/releases/appcompat)
     */
    const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"

    /**
     * [Paging](https://developer.android.com/jetpack/androidx/releases/paging)
     */
    const val paging = "androidx.paging:paging-runtime:${Versions.paging}"

    /**
     * [Core Kotlin Extensions](https://developer.android.com/kotlin/ktx#core)
     */
    const val ktxCore = "androidx.core:core-ktx:${Versions.ktxCore}"

    /**
     * [Fragment Navigation](https://developer.android.com/jetpack/androidx/releases/navigation)
     */
    const val fragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"

    const val uiKtx = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"

    /**
     * [Lifecycle](https://developer.android.com/jetpack/androidx/releases/lifecycle)
     */
    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"

    const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"

    const val commonJava8 = "androidx.lifecycle:lifecycle-common-java8:${Versions.lifecycle}"

    const val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleExtensions}"

    /**
     * [Android App Startup Runtime](https://mvnrepository.com/artifact/androidx.startup/startup-runtime)
     */
    const val startup = "androidx.startup:startup-runtime:${Versions.startup}"

    /**
     * [Multidex](https://developer.android.com/studio/build/multidex)
     */
    const val multidex = "androidx.multidex:multidex:${Versions.multidex}"

    /**
     * [ConstraintLayout](https://developer.android.com/jetpack/androidx/releases/constraintlayout)
     */
    const val constraintLayout =
        "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"

    /**
     * [RecyclerView](https://developer.android.com/jetpack/androidx/releases/recyclerview)
     */
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerViewVersion}"

    /**
     * [Camera Core](https://developer.android.com/jetpack/androidx/releases/camera)
     */
    const val cameraCore = "androidx.camera:camera-core:${Versions.cameraX}"

    /*
     * [CameraX Camera2 extensions](https://androidx.tech/artifacts/camera/camera-camera2/)
     */

    const val cameraCamera2 = "androidx.camera:camera-camera2:${Versions.cameraX}"
    /*
     * [CameraX Lifecycle library](https://androidx.tech/artifacts/camera/camera-lifecycle/)
     */

    const val cameraLifecycle = "androidx.camera:camera-lifecycle:${Versions.cameraX}"

    /*
     * [CameraX View class](https://mvnrepository.com/artifact/androidx.camera/camera-view?repo=google)
     */
    const val cameraView = "androidx.camera:camera-view:${Versions.cameraView}"

    /**
     * [Custom Tabs]( https://developer.chrome.com/docs/android/custom-tabs/overview/)
     */
    const val customTabs = "androidx.browser:browser:${Versions.customTabs}"

    /**
     * [Espresso Core](https://developer.android.com/training/testing/espresso/setup)
     */
    const val espressoCore = "androidx.test.espresso:espresso-core:${Versions.espressoCore}"
}
