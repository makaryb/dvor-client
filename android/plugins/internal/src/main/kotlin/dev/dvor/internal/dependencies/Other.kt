/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal.dependencies

import dev.dvor.internal.Versions

object Other {

    /**
     * [Material Components For Android](https://mvnrepository.com/artifact/com.google.android.material/material)
     */
    const val material = "com.google.android.material:material:${Versions.material}"

    /**
     * [Dokka](https://github.com/Kotlin/dokka)
     */
    const val dokka = "org.jetbrains.dokka:kotlin-as-java-plugin:${Versions.dokka}"

    /**
     * [Kotlinx DateTime](https://github.com/Kotlin/kotlinx-datetime)
     */
    const val kotlinxDatetime = "org.jetbrains.kotlinx:kotlinx-datetime:${Versions.kotlinxDatetime}"

    /**
     * [Kotlin multiplatform serialization](https://github.com/Kotlin/kotlinx.serialization)
     */
    const val serialization =
        "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.serialization}"

    /**
     * [Mockito](https://github.com/mockito/mockito)
     */
    const val mockitoCore = "org.mockito:mockito-core:${Versions.mockitoCore}"
}
