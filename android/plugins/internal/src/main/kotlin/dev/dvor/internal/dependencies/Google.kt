/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal.dependencies

import dev.dvor.internal.Versions

object Google {

    /**
     * [Play Services](https://developers.google.com/android/guides/setup)
     */
    const val playServices = "com.google.android.gms:play-services-auth:${Versions.playServices}"

    const val location = "com.google.android.gms:play-services-location:${Versions.location}"

    /**
     * [Google Maps](https://developers.google.com/maps/documentation/android-sdk/v3-client-migration)
     */
    const val maps = "com.google.android.libraries.maps:maps:${Versions.googleMaps}"

    const val mapsUtils =
        "com.google.maps.android:android-maps-utils-v3:${Versions.googleMapsUtils}"

    /**
     * [Google Maps KTX](https://github.com/googlemaps/android-maps-ktx)
     */
    const val mapsKtx = "com.google.maps.android:maps-v3-ktx:${Versions.googleMapsKtx}"

    const val mapsKtxUtils = "com.google.maps.android:maps-utils-v3-ktx:${Versions.googleMapsKtx}"
}
