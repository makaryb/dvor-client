/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal.dependencies

import dev.dvor.internal.Versions

object Di {
    /**
     * [Hilt Compiler](https://developer.android.com/training/dependency-injection/hilt-jetpack#workmanager)
     */
    const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.hilt}"

    /**
     * [Hilt Android](https://mvnrepository.com/artifact/com.google.dagger/hilt-android)
     */
    const val hiltAndroid = "com.google.dagger:hilt-android:${Versions.hiltCore}"

    /**
     * [Hilt Processor](https://mvnrepository.com/artifact/com.google.dagger/hilt-compiler)
     */
    const val daggerHiltCompiler = "com.google.dagger:hilt-compiler:${Versions.hiltCore}"

    /**
     * [Testing](https://developer.android.com/training/dependency-injection/hilt-testing)
     */
    const val testing = "com.google.dagger:hilt-android-testing:${Versions.hiltCore}"
}
