package dev.dvor.desktop

import androidx.compose.desktop.Window
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.IntSize
import dev.dvor.theming.DvorTheme

fun main(args: Array<String>) = Window(
    title = "Dvor-Client",
    size = IntSize(900, 1000),
    resizable = false
) {
    DvorTheme(isDarkMode = true) {
        Scaffold(
            modifier = Modifier
                .fillMaxSize()
                .background(color = MaterialTheme.colors.background)
        ) {
            Button(
                modifier = Modifier,
                onClick = {
                    //
                },
                enabled = true
            ) {
                Text(text = "Hello Multiplatform!")
            }
        }
    }
}
