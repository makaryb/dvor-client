rootProject.name = "dvor-client"
include(":theming")
include(":extensions")
include(":components")
include(":android")
include(":data")
include(":desktop")
includeBuild("android/plugins/internal")
