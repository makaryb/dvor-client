import org.jetbrains.compose.compose

typealias and = dev.dvor.internal.Android

plugins {
    id("com.android.library")
    kotlin("multiplatform")
    id("org.jetbrains.compose")

    id("internal")
}

group = "dev.dvor"
version = "0.1.0"

android {
    compileSdk = and.compileSdk

    defaultConfig {
        minSdk = and.minSdk
        targetSdk = and.targetSdk
        buildToolsVersion = and.buildTools
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    sourceSets {
        named("main") {
            manifest.srcFile("src/androidMain/AndroidManifest.xml")
        }
    }

    configurations {
        create("androidTestApi")
        create("androidTestDebugApi")
        create("androidTestReleaseApi")
        create("testApi")
        create("testDebugApi")
        create("testReleaseApi")
    }
}

kotlin {
    android()
    jvm("desktop")

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(compose.runtime)
                implementation(compose.foundation)
                implementation(compose.material)
            }
        }
    }
}
