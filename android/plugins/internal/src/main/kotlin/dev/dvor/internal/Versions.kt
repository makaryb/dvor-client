/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal

object Versions {
    // jetpack
    const val ktxCore = "1.5.0"
    const val appcompat = "1.3.0"

    // coroutines
    const val coroutines = "1.4.1"

    // compose
    const val compose = "1.0.0-beta06"
    const val activityCompose = "1.3.0-rc1"
    const val viewModelCompose = "1.0.0-alpha07"
    const val navigationCompose = "2.3.5"
    const val constraintLayoutCompose = "1.0.0-alpha08"
    const val pagingCompose = "1.0.0-alpha09"
    const val paging = "3.0.0"
    const val accompanist = "0.13.0"

    // android
    const val material = "1.1.0"
    const val constraintLayoutVersion = "2.0.4"
    const val recyclerViewVersion = "1.1.0"
    const val navigation = "2.3.3"
    const val lifecycle = "2.3.1"
    const val lifecycleExtensions = "2.2.0"

    // di
    const val hilt = "1.0.0"
    const val hiltCore = "2.36"
    const val hiltComposeNavigation = "1.0.0-alpha03"

    // logging
    const val timber = "4.7.1"

    // architecture components
    const val startup = "1.0.0"

    // test
    const val mockWebServer = "4.9.1"
    const val mockitoCore = "3.10.0"

    // room
    const val room = "2.3.0"

    // retrofit2
    const val retrofit2 = "2.9.0"
    const val gson = "2.8.7"
    const val interceptor = "4.9.1"

    // jUnit
    const val junitVersion = "4.13.2"

    // jdkDesugar
    const val jdkDesugar = "1.0.9"

    // Multidex
    const val multidex = "2.0.1"

    // Google Maps
    const val googleMaps = "3.1.0-beta"
    const val googleMapsUtils = "2.0.3"
    const val googleMapsKtx = "3.1.0"

    // OSMdroid
    const val osmdroid = "6.1.10"
    const val osmBonusPack = "6.6.0"

    // Firebase
    const val firebaseBOM = "28.2.0"
    const val firebaseAuth = "20.0.2"
    const val firebaseGradle = "2.4.1"
    const val firebaseStorage = "19.1.1"
    const val firebaseDatabase = "19.2.1"

    // Google
    const val playServices = "19.0.0"
    const val location = "17.0.0"

    // Accompanist
    const val glide = "0.10.0"
    const val coil = "0.10.0"
    const val insets = "0.10.0"
    const val swipeRefresh = "0.10.0"

    // PDFtron
    const val pdftron = "9.0.1"

    // CameraX
    const val cameraX = "1.0.0"
    const val cameraView = "1.0.0-alpha25"

    // other
    const val dokka = "1.4.32"
    const val sandwich = "1.1.0"
    const val serialization = "1.2.1"
    const val customTabs = "1.3.0"
    const val kotlinxDatetime = "0.2.1"
    const val espressoCore = "3.3.0"
}
