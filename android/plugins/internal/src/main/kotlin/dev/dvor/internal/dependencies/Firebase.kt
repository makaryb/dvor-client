/*
 * Copyright 2021 Makary Boriskin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("unused")

package dev.dvor.internal.dependencies

import dev.dvor.internal.Versions

object Firebase {

    /**
     * [Firebase](https://firebase.google.com/docs/android/setup)
     */
    const val bom = "com.google.firebase:firebase-bom:${Versions.firebaseBOM}"

    const val auth = "com.google.firebase:firebase-auth:${Versions.firebaseAuth}"

    const val crashlytics = "com.google.firebase:firebase-crashlytics-ktx"

    const val analytics = "com.google.firebase:firebase-analytics-ktx"

    const val storage = "com.google.firebase:firebase-storage:${Versions.firebaseStorage}"

    const val realtimeDatabase =
        "com.google.firebase:firebase-database:${Versions.firebaseDatabase}"
}
