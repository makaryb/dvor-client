import java.io.FileInputStream
import java.util.Properties

typealias and = dev.dvor.internal.Android
typealias dep = dev.dvor.internal.Dependencies

plugins {
    id("com.android.application")
    kotlin("android")
    id("org.jetbrains.compose")

    id("internal")

    id("com.diffplug.spotless")

    id("io.gitlab.arturbosch.detekt")

    // Apply the Google Services plugin (if it's not there already).
    id("com.google.gms.google-services")
    // Add the Firebase Crashlytics plugin.
    id("com.google.firebase.crashlytics")

    kotlin("kapt")
    kotlin("plugin.serialization")

    id("kotlin-parcelize")
}

group = "dev.dvor"
version = "0.1.0"

repositories {
    google()
}

spotless {
    kotlin {
        target("**/*.kt")
        licenseHeaderFile("${project.rootProject.projectDir}/spotless.license")
    }
    kotlinGradle {
        target("*.gradle.kts")
        ktlint()
    }
}

val fis = FileInputStream(rootProject.file("local.properties"))
val properties = Properties()
properties.load(fis)

android {
    defaultConfig {
        multiDexEnabled = true
    }

    compileSdk = and.compileSdk
    buildToolsVersion = and.buildTools

    val versioningFile = file("version.properties")

    if (versioningFile.canRead()) {

        val versionsInputStream = FileInputStream(versioningFile)
        val versionProps = Properties()
        versionProps.load(versionsInputStream)

        val name = versionProps.getProperty("VERSION_NAME")
        val code = versionProps.getProperty("VERSION_CODE").toInt() + 1
        versionProps.setProperty("VERSION_CODE", code.toString())
        versionProps.store(versioningFile.writer(), null)

        defaultConfig {
            applicationId = "dev.dvor.android"

            minSdk = and.minSdk
            targetSdk = and.targetSdk

            versionName = name
            versionCode = code

            testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
            vectorDrawables {
                useSupportLibrary = true
            }

            manifestPlaceholders["googleMapsKey"] =
                properties.getProperty("google.maps.key", "")
            manifestPlaceholders["pdftronLicenseKey"] =
                properties.getProperty("pdftrone.license.key", "")
        }

        signingConfigs {
            create("release") {
                keyAlias = "key"
                keyPassword = properties.getProperty("my.signing.release.key", "")
                storeFile = file("devops_key.jks")
                storePassword = properties.getProperty("my.signing.release.key", "")
                enableV2Signing = false
            }
        }

        buildTypes {
            getByName("release") {
                isMinifyEnabled = false
                proguardFiles(
                    getDefaultProguardFile(
                        "proguard-android-optimize.txt"
                    ),
                    "proguard-rules.pro"
                )
                signingConfig = signingConfigs.getByName("release")
            }
        }
    } else {
        throw GradleException("Couldn't read version.properties!")
    }

    increaseVersionCode()

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    packagingOptions {
        excludes += "/META-INF/AL2.0"
        excludes += "/META-INF/LGPL2.1"
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

fun increaseVersionCode() {
    gradle.taskGraph.whenReady {
        if (this.hasTask(":android:assembleRelease")) {
            val versioningFile = file("version.properties")
            if (versioningFile.canRead()) {
                val versionsInputStream = FileInputStream(versioningFile)
                val versionProps = Properties()
                versionProps.load(versionsInputStream)

                val buildVersion = versionProps.getProperty("VERSION_BUILD").toInt() + 1
                val codeVersion = versionProps.getProperty("VERSION_CODE").toInt() + 1

                versionProps.setProperty("VERSION_BUILD", buildVersion.toString())
                versionProps.setProperty("VERSION_CODE", codeVersion.toString())

                versionProps.store(versioningFile.writer(), null)
            } else {
                throw GradleException("Could not read version.properties!")
            }
        }
    }
}

tasks.create("doIncrementVersionCode") {
    group = "versioning"
    description = "Increments the version code to make the app ready for next release."
    doLast {
        println("Incrementing the Version Code") // alert
        increaseVersionCode()
    }
}

dependencies {
    api(project(":components"))
    api(project(":data"))

    implementation(Dependencies.kotlin)

    implementation(compose.ui)
    implementation(compose.material)

    implementation(Dependencies.Compose.uiTooling)

    dep.compose.apply { // https://developer.android.com/jetpack/compose
        implementation(activity)
        implementation(viewModel)
        implementation(navigation)
        implementation(constraintLayout)
        implementation(paging)

        implementation(foundation)
        implementation(viewBinding)
        implementation(runtime)
        implementation(runtimeLivedata)
        implementation(rxJava2)
        implementation(material)
        implementation(materialIconsCore)
        implementation(icons)
        implementation(uiUtil)
        implementation(layout)
        implementation(tooling)

        implementation(hiltNavigation)

        androidTestImplementation(uiTest)
        androidTestImplementation(uiTestJunit4)
        debugImplementation(uiTestManifest)
    }

    implementation(Dependencies.Accompanist.uiController)
    implementation(Dependencies.Accompanist.coil)

    dep.accompanist.apply { // https://google.github.io/accompanist/
        implementation(glide)

        implementation(swipeRefresh)
        implementation(insets)
    }

    dep.androidX.apply { // https://developer.android.com/jetpack/androidx
        implementation(appcompat)
        implementation(paging)
        implementation(ktxCore)
        implementation(fragment)
        implementation(uiKtx)

        implementation(viewModel)
        implementation(liveData)
        implementation(commonJava8)
        implementation(lifecycle)

        implementation(startup)
        implementation(multidex)

        implementation(constraintLayout)
        implementation(recyclerView)

        implementation(cameraCore)
        implementation(cameraCamera2)
        implementation(cameraLifecycle)
        implementation(cameraView)

        implementation(customTabs)

        androidTestImplementation(espressoCore)
    }

    dep.coroutines.apply { // https://github.com/Kotlin/kotlinx.coroutines
        api(core)
        api(android)
        implementation(test)
    }

    dep.hilt.apply { // https://dagger.dev/hilt/
        implementation(hiltAndroid)
        kapt(hiltCompiler)
        kapt(daggerHiltCompiler)
        kaptAndroidTest(daggerHiltCompiler)
        androidTestImplementation(testing)
    }

    dep.room.apply { // https://developer.android.com/jetpack/androidx/releases/room
        implementation(runtime)
        implementation(ktx)
        kapt(compiler)
        androidTestImplementation(testing)
    }

    dep.google.apply { // https://developers.google.com/android/guides/setup
        implementation(playServices)
        implementation(location)

        implementation(maps)
        implementation(mapsUtils)
        implementation(mapsKtx)
        implementation(mapsKtxUtils)
    }

    dep.firebase.apply { // https://firebase.google.com/docs/android/setup
        implementation(auth)
        implementation(
            platform(bom)
        )
        implementation(crashlytics)
        implementation(analytics)
        implementation(storage)
        implementation(realtimeDatabase)
    }

    dep.osmdroid.apply { // https://github.com/osmdroid/osmdroid
        implementation(osmdroidMain)
        implementation(osmdroidBonusPack)
    }

    dep.pdfTron.apply { // https://www.pdftron.com/documentation/android/faq/standard-version/
        implementation(standard)
        implementation(tools)
        implementation(demo)
    }

    dep.other.apply { // Miscellaneous required libraries
        implementation(material)
        implementation(serialization)
        implementation(kotlinxDatetime)
        testImplementation(mockitoCore)
    }

    dep.logging.apply { // https://medium.com/mindorks/better-logging-in-android-using-timber-72e40cc2293d
        implementation(timber)
    }
}
