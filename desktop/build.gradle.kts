import org.jetbrains.compose.compose

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

group = "dev.dvor"
version = "0.1.0"

dependencies {
    implementation(project(":components"))
    implementation(project(":data"))
    implementation(compose.desktop.currentOs)
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

compose.desktop {
    application {
        mainClass = "dev.dvor.desktop.MainKt"
    }
}
