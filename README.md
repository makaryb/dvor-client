Dvor Client
===================

<img src="readme/preview.png"  width="125" height="125">

Minimal Kotlin Multiplatform project with Jetpack Compose (Android) and Compose for Desktop.
Version 2 of client-side project (Moy Dvor).

Implementation of the application using the latest Android, Desktop Compose tech stack and the MoyDvor REST API - `myhoa.herokuapp.com`.

### Build

I use Android Studio Bumblebee 2021.1.1 Canary 3 (note: Java 11 is the minimum version required) with [Compose Multiplatform IDE Support](https://plugins.jetbrains.com/plugin/16541-compose-multiplatform-ide-support). 

But it also may build successfully with AS Arctic Fox 2020.3.1.

To run Android-part - use AS tools and your favourite emulator or real device. To run Desktop-part - run `./gradlew :desktop:RUN` in terminal, for example, in AS. 

Remember that you need `google.maps.key` in your `local.properties`. And `google-services.json ` in `~/android/...` (you can disable it by removing `plugin("com.google.gms.google-services")` in android-level `build.gradle.kts `).

### Architecture

* MVVM - [Guide to app architecture](https://developer.android.com/jetpack/guide)
* [Kotlin](https://kotlinlang.org/) 100%, [Coroutines](https://github.com/Kotlin/kotlinx.coroutines),
  [Flow](https://kotlinlang.org/docs/flow.html)


### Preview
<p align="center">
<img src="readme/dvor_screen_1.gif" width="32%"/>
<img src="readme/dvor_screen_2.gif" width="32%"/>
<img src="readme/dvor_screen_3.gif" width="32%"/>
</p>

### Libraries

... in progress

# License

```
Copyright 2021 Makary Boriskin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

